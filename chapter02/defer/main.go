package main

import "fmt"

func main() {
	for i := 0; i < 5; i++ {
		defer func(n int) {
			fmt.Printf("%d", n)
		}(i * 2)
	}

	defer fmt.Println("首先会被执行的defer")
	fmt.Println("defer们该出来了吧！")

}
