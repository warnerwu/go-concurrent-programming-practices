package main

import "fmt"

type User struct {
	Username string
	Email string
	Sex string
}

// 外围函数
func outFunc()  {
	// 延迟函数
	defer fmt.Println("中国你好！")
	fmt.Println("我爱你中国~")
}

func main() {
	// 调用函数
	outFunc()
	fmt.Println("程序运行该结束了")
}
