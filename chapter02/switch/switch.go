package main

import (
	"strings"
	"fmt"
	"os"
)

func main() {
	content := "Ruby         "


	switch lang := strings.TrimSpace(content); lang {
	case "Ruby":
		break
	case "Python":
		fmt.Println("An interpreted Language")
		os.Exit(0)
	case "C", "C++", "GO":
		fmt.Println("A complied Language")
		os.Exit(0)
	default:
		fmt.Println("Unknown Language")
		os.Exit(0)
	}

	fmt.Println("提前退出了")
}
