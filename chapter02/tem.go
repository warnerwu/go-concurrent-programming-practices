package chapter02

import "fmt"

type Talk interface {
	Hello(username string) string
	Talk(heard string) (saying string, end bool, err error)
}

type SimpleCN struct {
	name string
	talk Talk
}

func main() {
	var sp SimpleCN

	fmt.Printf("%v", sp)
}