package main

import (
	"fmt"
	"time"
)

func main() {
	timer := time.NewTimer(time.Second * 2)
	fmt.Printf("Present time: %v.\n", time.Now())
	// 通过定时器的字段C, 我们可以及时得到定时器到期的通知, 并对此作出响应
	expirationTime := <-timer.C
	fmt.Printf("Expiration time:%v.\n", expirationTime)
	fmt.Printf("Stop timer:%v.\n", timer.Stop())
}
