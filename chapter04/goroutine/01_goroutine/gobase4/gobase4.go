package main

import (
	"fmt"
	"time"
)

func main() {
	// Slice of the names
	names := []string{"Eric", "Harry", "Robert", "Jim", "Mark"}

	// Loop of the names
	for _, name := range names {
		go func() {
			fmt.Printf("Hello, %s\n", name)
		}()
		time.Sleep(time.Millisecond)
	}
}
