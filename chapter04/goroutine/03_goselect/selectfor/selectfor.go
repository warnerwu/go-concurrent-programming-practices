package main

import "fmt"

func main() {
	chanCap := 10
	intChan := make(chan int, chanCap)
	for i := 0; i < chanCap; i++ {
		intChan <- i
	}
	close(intChan)
	syncChan := make(chan struct{}, 1)
	go func() {
	Loop:
		for {
			select {
			case e, ok := <-intChan:
				if !ok {
					fmt.Println("End.")
					break Loop
				}
				fmt.Printf("Received:%v\n", e)
			}
		}
		syncChan <- struct{}{}
	}()
	<-syncChan
}
