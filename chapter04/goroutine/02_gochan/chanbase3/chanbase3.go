package main

import (
	"fmt"
	"time"
)

// 数据通道
var strChan = make(chan string, 3)

func main() {
	// 同步信号通道
	syncChan1 := make(chan struct{}, 1)
	syncChan2 := make(chan struct{}, 2)
	// 用于演示接收操作
	go receive(strChan, syncChan1, syncChan2)
	// 用于演示发送操作
	go send(strChan, syncChan1, syncChan2)
	// 获取信号
	<-syncChan2
	<-syncChan2
}

// 接收
func receive(strChan <-chan string, syncChan1 <-chan struct{}, syncChan2 chan<- struct{}) {
	// 从通道接收一个信号值
	<-syncChan1

	// 输出接收提示
	fmt.Println("Received a sync and wait a seconds... [receiver]")

	// 睡眠一秒钟
	time.Sleep(time.Second)

	for elem := range strChan {
		fmt.Println("Received:", elem, "[receiver]")
	}

	// 输出接收数据完毕提示
	fmt.Println("Stopped. [receiver]")

	// 发送信号值到信号通道2
	syncChan2 <- struct{}{}
}

// 发送
func send(strChan chan<- string, syncChan1 chan<- struct{}, syncChan2 chan<- struct{}) {
	for _, elem := range []string{"a", "b", "c", "d"} {
		strChan <- elem
		fmt.Println("Sent:", elem, "[sender]")
		if elem == "c" {
			syncChan1 <- struct{}{}
			fmt.Println("Sent a sync signal. [sender]")
		}
	}
	// 输出等待时间提示
	fmt.Println("Wait 2 seconds... [sender]")
	// 睡眠2秒钟
	time.Sleep(time.Second * 2)
	// 关闭通道
	close(strChan)
	// 发送信号值到信号通道2
	syncChan2 <- struct{}{}
}
