package main

import (
	"fmt"
	"time"
)

//Counter
type Counter struct {
	count int
}

func (counter *Counter) String() string {
	return fmt.Sprintf("{count:%d}", counter.count)
}

var mapChan = make(chan map[string]*Counter, 1)

func main() {
	syncChan := make(chan struct{}, 2)

	// 用于演示接收操作
	go func() {
		for {
			if elem, ok := <-mapChan; ok {
				counter := elem["count"]
				counter.count++
			} else {
				break
			}
		}
		fmt.Println("Stopped. [receiver]")
		syncChan <- struct{}{}
	}()

	// 用于演示发送操作
	go func() {
		countMap := map[string]*Counter{
			"count": {},
		}

		for i := 0; i < 5; i++ {
			mapChan <- countMap
			time.Sleep(time.Millisecond)
			fmt.Printf("The count map:%v. [sender]\n", countMap)
		}
		close(mapChan)
		syncChan <- struct{}{}
	}()
	<-syncChan
	<-syncChan
}
