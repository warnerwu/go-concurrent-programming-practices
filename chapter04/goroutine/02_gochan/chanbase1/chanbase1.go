package main

import (
	"fmt"
	"time"
)

// 字符串通道
var strChan = make(chan string, 3)

func main() {
	// 同步通道1
	syncChan1 := make(chan struct{}, 1)
	// 同步通道2
	syncChan2 := make(chan struct{}, 2)

	// 用于演示接收操作
	go func() {
		// 从通道接收一个元素值
		<-syncChan1

		// 接收到同步信号并等待一秒…
		fmt.Println("Received a sync signal and wait a second ... [receiver]")

		// 等待一秒
		time.Sleep(time.Second)

		for {
			// 接收元素值
			if elem, ok := <-strChan; ok {
				fmt.Println("Received:", elem, "[receiver]")
			} else {
				break
			}
		}

		// 停止接收
		fmt.Println("Stopped. [receiver]")

		// 发送元素值到Chan
		syncChan2 <- struct{}{}
	}()

	// 用于演示发送操作
	go func() {
		for _, elem := range []string{"a", "b", "c", "d"} {
			// 发送元素值到通道
			strChan <- elem
			// 输出发送元素值到通道提示
			fmt.Println("Sent:", elem, "[sender]")
			// 如果元素值为String类型c
			if elem == "c" {
				// 发送元素值到同步通道1
				syncChan1 <- struct{}{}
				// 输出发送元素值到通道提示
				fmt.Println("Sent a sync signal. [sender]")
			}
		}

		// 发送元素值到通道完毕输出等待提示信息
		fmt.Println("Wait 2 seconds ... [sender]")
		// 睡眠2秒
		time.Sleep(time.Second * 2)
		// 关闭通道
		close(strChan)
		// 发送值到同步通道2
		syncChan2 <- struct{}{}
	}()

	// 接收通道2元素值
	<-syncChan2
	<-syncChan2
}
