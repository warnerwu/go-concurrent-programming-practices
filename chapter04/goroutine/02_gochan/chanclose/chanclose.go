package main

import (
	"fmt"
)

func main() {
	dataChan := make(chan int, 5)
	syncChan1 := make(chan struct{}, 1)
	syncChan2 := make(chan struct{}, 2)

	// 用于演示接收操作
	go func() {
		// 从同步通道接收一个值
		<-syncChan1

		// 迭代从通道取出元素值
		for {
			if elem, ok := <-dataChan; ok {
				fmt.Printf("Received:%d [receiver]\n", elem)
			} else {
				break
			}
		}

		// 输出接收完成提示信息
		fmt.Println("Done. [receiver]")

		// 发送通道信号
		syncChan2 <- struct{}{}
	}()

	// 用于演示发送操作
	go func() {
		// 发送元素值到通道
		for i := 0; i < 5; i++ {
			dataChan <- i
			fmt.Printf("Sent:%d [sender]\n", i)
		}
		// 关闭通道
		close(dataChan)
		// 发送信号到同步通道1
		syncChan1 <- struct{}{}
		// 发送数据完成提示
		fmt.Println("Done.[sender]")
		// 发送信号到同步通道2
		syncChan2 <- struct{}{}
	}()

	<-syncChan2
	<-syncChan2
}
