package main

import (
	"os"
	"os/signal"
	"fmt"
	"syscall"
			)

func main() {
	sigRcev := make(chan os.Signal, 1)

	// igs := []os.Signal{syscall.SIGINT, syscall.SIGQUIT}

	signal.Notify(sigRcev, syscall.SIGKILL, syscall.SIGSTOP)

	for sig := range sigRcev {
		fmt.Printf("Received a signal: %s\n", sig)
	}
}
