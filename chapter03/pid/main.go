package main

import (
	"os"
	"fmt"
)

func main() {
	pid := os.Getpid()
	ppid := os.Getppid()

	fmt.Printf("pid is:%d, ppid is:%d", pid, ppid)
}
