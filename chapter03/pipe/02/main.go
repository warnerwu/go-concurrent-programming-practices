package main

import (
	"os/exec"
	"fmt"
	"bytes"
	"io"
)

func main() {
	cmd0 := exec.Command("echo", "-n", "My first command comes from golang.中国上下五千年！")

	stdout0, err := cmd0.StdoutPipe()

	if err != nil {
		fmt.Printf("Error:Couldn't obtian the stdout pipe for command No.0:%s\n", err)
		return
	}

	if err := cmd0.Start(); err != nil {
		fmt.Printf("Error:The command No.0 can not be startup:%s\n", err)
		return
	}

	var outputBuf0 bytes.Buffer

	for {
		tempOutput := make([]byte, 5)

		n, err := stdout0.Read(tempOutput)

		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Printf("Error: Couldn't read from the pipe:%s\n", err)
				return
			}
		}
		if n > 0 {
			outputBuf0.Write(tempOutput[:n])
		}
	}

	fmt.Printf("%s\n", outputBuf0.String())
}

