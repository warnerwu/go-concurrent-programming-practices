package main

import (
	"os/exec"
	"fmt"
			"bufio"
)

func main() {
	cmd0 := exec.Command("echo", "-n", "My first command comes from golang.中国上下五千年！")

	stdout0, err := cmd0.StdoutPipe()

	if err != nil {
		fmt.Printf("Error:Couldn't obtian the stdout pipe for command No.0:%s\n", err)
		return
	}

	if err := cmd0.Start(); err != nil {
		fmt.Printf("Error:The command No.0 can not be startup:%s\n", err)
		return
	}

	outputBuf0 := bufio.NewReader(stdout0)

	output0, _, err := outputBuf0.ReadLine()

	if err != nil {
		fmt.Printf("Error: Could't read data from the pipe:%s\n", err)
		return
	}

	fmt.Printf("%s\n", string(output0))
}
