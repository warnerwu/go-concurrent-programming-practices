package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

func main() {
	fileBasedPipe()
	inMemorySyncPipe()
}

func fileBasedPipe() {
	// 创建命名管道
	reader, writer, err := os.Pipe()

	// 创建命名管道, 可能发生错误
	if err != nil {
		fmt.Printf("Error: Couldn't create the named pipe: %s\n", err)
	}

	// 异步的读取管道数据
	go func() {
		output := make([]byte, 100)
		n, err := reader.Read(output)
		if err != nil {
			fmt.Printf("Error: Couldn't read data from the named pipe: %s\n", err)
		}
		fmt.Printf("Read %d byte(s). [file-based pipe] values(%s)\n", n, string(output))
	}()

	// 初始化输入切片
	input := make([]byte, 26)

	// 循环向输入切片写入字节值
	for i := 65; i <= 90; i++ {
		input[i-65] = byte(i)
	}

	// 向管道写入数据
	n, err := writer.Write(input)

	// 向管道写入数据, 可能发生的错误
	if err != nil {
		fmt.Printf("Error: Couldn't write data to the named pipe: %s\n", err)
	}

	// 输出管道写入字节长度以及写入的字节内容
	fmt.Printf("Written %d byte(s). [file-based pipe] values(%s)\n", n, string(input))

	// 给程序一个执行时间, 要不然Go协程执行管道读取操作没有执行程序就结束了, 这个问题可以通过Channel解决
	time.Sleep(200 * time.Millisecond)
}

func inMemorySyncPipe() {
	// 创建内存管道
	reader, writer := io.Pipe()

	// 异步的读取管道数据
	go func() {
		output := make([]byte, 100)
		n, err := reader.Read(output)
		if err != nil {
			fmt.Printf("Error: Couldn't read data from the named pipe: %s\n", err)
		}
		fmt.Printf("Read %d byte(s). [in-memory pipe] values(%s)\n", n, string(output))
	}()

	// 初始化输入切片
	input := make([]byte, 26)

	// 循环向输入切片写入字节值
	for i := 65; i <= 90; i++ {
		input[i-65] = byte(i)
	}

	// 向管道写入数据
	n, err := writer.Write(input)

	// 向管道写入数据, 可能发生的错误
	if err != nil {
		fmt.Printf("Error: Couldn't write data to the named pipe: %s\n", err)
	}

	// 输出管道写入字节长度以及写入的字节内容
	fmt.Printf("Written %d byte(s). [in-memory pipe] values(%s)\n", n, string(input))

	// 给程序一个执行时间, 要不然Go协程执行管道读取操作没有执行程序就结束了, 这个问题可以通过Channel解决
	time.Sleep(200 * time.Millisecond)
}
